---
title: "General"
date: 2021-05-14T13:28:58+02:00
draft: false
homepage: "general"
---

Die Critical Mass (CM) ist eine weltweite stattfindende Aktion, bei der sich Radfahrer:innen zufällig und unorganisiert treffen, um mit gemeinsamen Fahrradtouren durch Städte ein Zeichen für mehr Radverkehr als umweltfreundliches Verkehrsmittel zu setzen. Die Teilnehmer:innen protestieren dabei für ihre Rechte als gleichberechtigte Verkehrsteilnehmer, für eine bessere Infrastruktur und gegen die Vormachtstellung des Autos in Städten.  

Bei dieser Aktionsform gibt es keine Organisator:innen oder Hauptverantwortliche. Jede:r Teilnehmer:in ist für sich selbst verantwortlich und trägt denselben Anteil zum Gelingen der CM bei.  

Wenn du interessiert bist, komm einfach zur nächsten CM vorbei, um zusammen in freundlicher und entspannter Atmosphäre durch die Stadt zu radeln.

---
**Dürfen die das?**

Ja das dürfen die! Wenn eine Gruppe Fahrradfahrer*innen von mindestens 15 Rädern gemeinsam die gleiche Strecke fährt, hat sie nicht mehr den Status einzelner Verkehrsteilnehmer. Sie ist statt dessen ein “Verband” und zählt als ein Verkehrsteilnehmer. Ein Verband darf eine gesamte Fahrspur einnehmen. Er ist nicht daran gebunden den Radweg zu benutzen und die Räder müssen nicht, wie sonst, hintereinander fahren. Ebenso darf ein Verband auch Ampeln geschlossen überqueren. Sobald also die:der erste im Verband eine noch grüne Ampel überfährt, darf der Rest des Verbandes auch bei gelb oder rot noch weiterfahren.

Die Critical Mass muss nicht angemeldet werden. Wenn sich mindestens 15 Fahrradfahrer:innen geplant oder zufällig treffen, um gemeinsam eine Ausfahrt zu machen, handelt es sich um einen Verband. Die Critical Mass ist daher keine Demo und muss auch nicht angemeldet werden.

Die rechtliche Grundlage bietet der {{< a-target-blank "§27 der StVO." "https://www.gesetze-im-internet.de/stvo_2013/__27.html" >}}

---

Du möchtest mitfahren, schaffst es aber nicht ganz pünktlich? Lad dir die {{< a-target-blank "Critical Maps-App" "https://www.criticalmaps.net/" >}} runter und schau, wo wir gerade sind!
