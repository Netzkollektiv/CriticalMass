---
title: "Rules"
date: 2021-05-14T13:29:07+02:00
draft: false
homepage: "rules"
---

Die CM-Rides sind unorganisiert und unhierarchisch. Damit die nächste CM gelingt und alle Freude am Fahren haben, beachte bitte die folgenden “Regeln”.

- Wir fahren im Verband. Das heißt wir bleiben nah beieinander. Sobald die:der erste bei Grün eine Ampel überfährt, folgt der Rest. Bei Rot oder Gelb wird natürlich an der Ampel gewartet.

- Wird der Verband auseinander gerissen, wird vorne im langsamen Tempo weitergefahren, sodass Lücken geschlossen werden können.

- Wir halten uns an die Verkehrsregeln. Wir fahren nicht auf Fußwegen.

- Achte darauf, dass dein Fahrrad sicher ist. Falls die CM im dunkeln stattfindet, achte darauf, dass dein Fahrrad ausreichend beleuchtet ist.

- Die:der erste gibt an wo es langgeht. Jede:r kann und darf vorne fahren.

- Wir fahren auf einer Spur. Für Krankenwagen oder andere Einsatzfahrzeuge wird ausreichend Platz gemacht!

- Habe Spaß und Freude an der CM. Erzähle deinen Freund:innen davon und lade sie ein mitzumachen.
