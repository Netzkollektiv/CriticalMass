---
title: "Links"
date: 2021-05-16T16:24:33+02:00
draft: false
homepage: "links"
---
### ADFC Halle
- {{< a-target-blank "Website" "https://www.adfc-sachsenanhalt.de/halle" >}}
- {{< a-target-blank "Radverkehrspolitik" "https://www.adfc-sachsenanhalt.de/adfc-sachsen-anhalt/halle-saale/politik/" >}}
- {{< a-target-blank "Radschnellweg Halle-Leipzig" "https://www.adfc-sachsenanhalt.de/adfc-sachsen-anhalt/halle-saale/radschnellweg-halle-leipzig/" >}}

### Stadt Halle
- {{< a-target-blank "Runder Tisch Radverkehr" "https://www.halle.de/de/Verwaltung/Stadtentwicklung/Verkehr-allgemein/Planung/Radverkehr/Runder-Tisch-Radverkehr/" >}}
